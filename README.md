# Hotels

Program to implement the logic of hotels, rooms, users and reservations.

#### Notes

- According to the project specification, the hotels should have the first 10 rooms as VIPs. This solution, instead, let the hotel have any number of VIP rooms (up to the room number limit).
