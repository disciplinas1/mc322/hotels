package mc322.lab03.hotels;

class Room implements Comparable<Room> {

    // Properties
    private int number;
    private RoomType type;
    private boolean smokersAllowed;
    private boolean airConditioner;
    private boolean occupied;

    // Enumerators
    public enum RoomType {
        VIP("VIP"),
        STANDARD("Standard");

        String label;

        private RoomType(String label) {
            this.label = label;
        }
    }

    // Initializer
    public Room(int number, RoomType type, boolean smokersAllowed, boolean airConditioner) {
        this.number = number;
        this.type = type;
        this.smokersAllowed = smokersAllowed;
        this.airConditioner = airConditioner;
        this.occupied = false;
    }

    // Methods
    public void showInformation(String indent, boolean detailed) {
        System.out.println(indent + "Room number " + this.number);
        System.out.println(indent + "  Type: " + this.type.label);
        if (detailed) {
            System.out.println(indent + "  Occupied? " + (this.occupied ? "Yes" : "No"));
            System.out.println(indent + "  Allow smokers? " + (this.smokersAllowed ? "Yes" : "No"));
            System.out.println(indent + "  Has air conditioner? " + (this.airConditioner ? "Yes" : "No"));
        }
    }

    void checkIn() {
        this.occupied = true;
    }

    void checkOut() {
        this.occupied = false;
    }

    public boolean isOccupied() {
        return this.occupied;
    }

    public boolean allowSmokers() {
        return this.smokersAllowed;
    }

    public RoomType getType() {
        return this.type;
    }

    int getNumber() {
        return this.number;
    }

    @Override
    public int compareTo(Room other) {
        return this.number - other.number;
    }
}
