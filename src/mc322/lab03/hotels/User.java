package mc322.lab03.hotels;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

class User {

    // Static Properties
    private static String dateFormat = "dd/MM/yyyy";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(User.dateFormat);

    // Properties
    private String name;
    private String cpf;
    private LocalDate birthDate;
    private Gender gender;
    private boolean smoker;
    private double moneyBalance;

    // Enumerators
    public enum Gender {
        MALE("Male"),
        FEMALE("Female"),
        OTHER("Other");

        String label;

        Gender(String label) {
            this.label = label;
        }
    }

    // Initializer
    public User(String name, String cpf, String birthDateString, Gender gender, boolean smoker) {
        this.name = name;
        this.cpf = cpf;
        this.gender = gender;
        this.smoker = smoker;

        try {
            LocalDate birthDate = LocalDate.parse(birthDateString, User.formatter);
            this.birthDate = birthDate;
        } catch (Exception e) {
            System.out.println("Error: invalid date string: '" + birthDateString + "' Use format " + User.dateFormat);
        }
    }

    // Methods
    public void showInformation(String indent, boolean detailed) {
        System.out.println(indent + "User: " + this.name);
        System.out.println(indent + "  Money balance: " + this.getMoneyBalanceAsString());
        if (detailed) {
            System.out.println(indent + "  CPF: " + this.cpf);
            System.out.println(indent + "  Birth: " + this.getBirthDateAsString());
            System.out.println(indent + "  Gender: " + this.gender.label);
            System.out.println(indent + "  Smoker? " + (this.smoker ? "Yes" : "No"));
        }
    }

    public void depositMoney(double value) {
        if (value < 0) {
            System.out.println("Error: trying to deposit negative amount to user " + this.name);
            return;
        }

        this.moneyBalance += value;
    }

    public boolean payMoney(double value) {
        if (value < 0) {
            System.out.println("Error: trying to withdraw negative amount from user " + this.name);
            return false;
        }

        if (value > this.moneyBalance) {
            System.out.println("Error: user " + this.name + " has not enough money to pay.");
            return false;
        }

        this.moneyBalance -= value;
        return true;
    }

    public boolean isSmoker() {
        return this.smoker;
    }

    private String getMoneyBalanceAsString() {
        return String.format("$%.2f", this.moneyBalance);
    }

    private String getBirthDateAsString() {
        return this.birthDate.format(User.formatter);
    }
}
