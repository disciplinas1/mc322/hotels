package mc322.lab03.hotels;

public class Booking {

    // Main Method
    public static void main(String args[]) {

        // Creating hotels
        Hotel praiaHotel = new Hotel("Praia Tropical", "Rua Tajubá, 201 - Florianópolis, SC", "3225-8997", 100.00, 900.00);
        Hotel campoHotel = new Hotel("Campo florestal", "Rua Monteiro, 456 - Goiânia, GO", "3654-8974", 50.00, 2000.00);

        // Creating users
        User roberci = new User("Roberci Silva", "784245698-21", "12/04/1996", User.Gender.MALE, true);
        roberci.depositMoney(1000.00);

        User marcela = new User("Marcela Domingues", "269784061-45", "22/07/1998", User.Gender.FEMALE, false);
        marcela.depositMoney(2000.00);

        // Creating rooms
        praiaHotel.addRoom(2, Room.RoomType.VIP, true, true);
        praiaHotel.addRoom(22, Room.RoomType.STANDARD, false, false);
        praiaHotel.addRoom(87, Room.RoomType.STANDARD, false, false);
        campoHotel.addRoom(13, Room.RoomType.STANDARD, false, false);
        campoHotel.addRoom(87, Room.RoomType.STANDARD, false, false);
        campoHotel.addRoom(99, Room.RoomType.STANDARD, true, false);

        // Checking in
        Booking.checkIn(roberci, praiaHotel, 2, 1);
        Booking.checkIn(marcela, campoHotel, 13, 4);
        Booking.checkIn(roberci, praiaHotel, 87, 1);
        Booking.cancelCheckIn(marcela, praiaHotel, 22, 4);
        Booking.checkIn(roberci, campoHotel, 99, 1);
        Booking.cancelCheckIn(roberci, campoHotel, 99, 1);
        Booking.checkIn(marcela, campoHotel, 87, 1);
    }

    // Static Methods
    public static boolean checkIn(User user, Hotel hotel, int roomNumber, int days) {
        if (days < 1) {
            System.out.println("Error: trying to check in with invalid duration: " + days + " days.");
            return false;
        }

        double price = hotel.getPrice(roomNumber, days);
        if (price == 0) {
            return false;
        }

        if (!user.payMoney(price)) {
            return false;
        }

        if(!hotel.checkIn(roomNumber, user)) {
            user.depositMoney(price);
            return false;
        }

        return true;
    }

    public static boolean checkOut(Hotel hotel, int roomNumber) {
        return hotel.checkOut(roomNumber);
    }

    public static boolean cancelCheckIn(User user, Hotel hotel, int roomNumber, int days) {
        if (!hotel.checkOut(roomNumber)) {
            return false;
        }

        double price = hotel.getPrice(roomNumber, days);
        if (price == 0) {
            return false;
        }

        user.depositMoney(0.70 * price);
        return true;
    }
}
