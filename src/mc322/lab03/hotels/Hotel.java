package mc322.lab03.hotels;

import java.util.TreeSet;
import java.util.stream.Collectors;

class Hotel {

    // Static Properties
    static int maxRooms = 100;

    // Properties
    private String name;
    private String address;
    private String phone;
    private TreeSet<Room> rooms;
    private double standardPrice;
    private double vipPrice;

    // Initializer
    public Hotel(String name, String address, String phone, double standardPrice, double vipPrice) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.standardPrice = standardPrice;
        this.vipPrice = vipPrice;
        this.rooms = new TreeSet<Room>();
    }

    // Methods
    public void showInformation(String indent, boolean showFreeRooms) {
        System.out.println(indent + this.name);
        System.out.println(indent + "  Address: " + this.address);
        System.out.println(indent + "  Phone: " + this.phone);
        System.out.println(indent + "  Standard room price: " + Hotel.priceToString(this.standardPrice));
        System.out.println(indent + "  VIP room price: " + Hotel.priceToString(this.vipPrice));
        if (showFreeRooms) {
            TreeSet<Room> freeRooms = this.getFreeRooms();
            int total = freeRooms.size();
            System.out.println(indent + "  Free rooms (" + total + " total):");
            for (Room room: freeRooms) {
                room.showInformation("  ", false);
            }
        }
    }

    public void showRooms(String indent) {
        for (Room room: this.rooms) {
            room.showInformation(indent, true);
        }
    }

    boolean checkIn(int roomNumber, User user) {
        Room room = this.getRoom(roomNumber);
        if (room == null) {
            return false;
        }

        if (room.isOccupied()) {
            System.out.println("Error: trying to check in to occupied room " + roomNumber);
            return false;
        }

        if (user.isSmoker() && !room.allowSmokers()) {
            System.out.println("Error: trying to check in smoker to non smokers room " + roomNumber);
            return false;
        }

        room.checkIn();
        return true;
    }

    boolean checkOut(int roomNumber) {
        Room room = this.getRoom(roomNumber);
        if (room == null) {
            return false;
        }

        if (!room.isOccupied()) {
            System.out.println("Error: trying to check out to empty room " + roomNumber);
            return false;
        }

        room.checkOut();
        return true;
    }

    public double getPrice(int roomNumber, int days) {
        Room room = this.getRoom(roomNumber);
        if (room == null) {
            return 0;
        }

        if (days < 1) {
            return 0;
        }

        switch (room.getType()) {
            case VIP:
                return this.vipPrice;
            default:
                return this.standardPrice;
        }
    }

    public void addRoom(int number, Room.RoomType type, boolean smokersAllowed, boolean airConditioner) {
        if (this.rooms.size() >= Hotel.maxRooms) {
            System.out.println("Error: trying to add room to full hotel: " + this.name);
            return;
        }

        if (this.hasRoom(number)) {
            System.out.println("Error: trying to add second room numbered " + number + " to hotel: " + this.name);
            return;
        }

        Room newRoom = new Room(number, type, smokersAllowed, airConditioner);
        this.rooms.add(newRoom);
    }

    private TreeSet<Room> getFreeRooms() {
        TreeSet<Room> freeRooms = new TreeSet<Room>(
            this.rooms
                .stream()
                .filter(room -> !room.isOccupied())
                .collect(Collectors.toSet())
        );

        return freeRooms;
    }

    private Room getRoom(int roomNumber) {
        for (Room room: this.rooms) {
            if (room.getNumber() == roomNumber) {
                return room;
            }
        }

        System.out.println("Error: no room with number " + roomNumber + " in hotel " + this.name);
        return null;
    }

    private boolean hasRoom(int roomNumber) {
        for (Room room: this.rooms) {
            if (room.getNumber() == roomNumber) {
                return true;
            }
        }

        return false;
    }

    // Static Methods
    private static String priceToString(double price) {
        return String.format("$%.2f", price);
    }
}
